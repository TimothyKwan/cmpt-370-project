var nextMove = [-1, -1];
var turn = -1;
function setNext(x, z) {
    nextMove = [x, z];
}

function getNext() {
    return nextMove;
}

function setTurnTo(direction) {
    turn = direction;
}

function getTurnTo() {
    return turn;
}
