var counter = 0;

function patrol(state, santa, gridSize) {
    //state for the player, santa for the santa object, gridSize for how fast it moves
    let playerPosition = vec2.fromValues(state.camera.position[0], state.camera.position[2]);//get the player's position
    let playerGrid = vec2.fromValues(Math.floor(playerPosition[0]), Math.floor(playerPosition[1]));//get the player's grid
    if (counter < 1000) {
        //console.log("player at " + playerPosition);
        //console.log("santa at " + santaPos);
        counter++;
    }
    let santaPos = vec2.fromValues(santa.model.position[0], santa.model.position[2]);//get the santa's position
    const step = 0.01;//round up the deltaTime and get the step
    var checkStep = step * 2;
    let direction = chasePlayer(state, santaPos, state.map, step*2);//


    switch (direction) {
        case 0:
            vec3.add(santa.model.position, santa.model.position, vec3.fromValues(step, 0.0, 0.0));
            break;
        case 1:
            vec3.add(santa.model.position, santa.model.position, vec3.fromValues(0.0, 0.0, step));
            break;
        case 2:
            vec3.add(santa.model.position, santa.model.position, vec3.fromValues(-step, 0.0, 0.0));
            break;
        case 3:
            vec3.add(santa.model.position, santa.model.position, vec3.fromValues(0.0, 0.0, - step));
            break;
        case -1:
            break;
    }

    if (checkPlayer(playerGrid, santaPos)) {
        //console.log("player is reached");
        state.gameOver = true;
    }
    if (direction != -1 && Math.abs(santaPos[0] - getNext()[0]) <= checkStep && Math.abs(santaPos[1] - getNext()[1]) <= checkStep) {
        console.log(true);
        mat4.rotateY(santa.model.rotation, santa.model.rotation, ((state.santaFace - direction) * Math.PI / 2) * -1);
        vec3.round(santa.model.position, santa.model.position);
    }
    if (santaPos[0] > state.map.length || santaPos[0] < 0 || santaPos[1] > state.map[0].length || santaPos[1] < 0) {//if santa when outside of the map
        //santa.model.position = vec3.fromValues(8.0, 0.0, 2.0);
        vec3.set(santa.model.position, 8.0, 0.0, 2.0);
        direction = chasePlayer(state, santaPos, state.map, step * 3);
        console.log(direction == state.santaFace);
    }
    state.santaFace = direction;
}


function chasePlayer(state, santaPos, map, step) {
    let direction = state.santaFace;//get direction from the state
    var santaObject = state.objects[findObjectByName(state, "santa")];
    let playerPosition = vec2.fromValues(Math.floor(state.camera.position[0]), Math.floor(state.camera.position[2]));// get the player grid
    let santaGrid = vec2.fromValues(Math.floor(santaPos[0]), Math.floor(santaPos[1]));// get the santa grid
    let move = vec2.fromValues(getNext()[0], getNext()[1]);
    if (move[0] == -1 && move[1] == -1) {
        console.log("initialize moveNext")
        setNext(Math.floor(santaPos[0]), Math.floor(santaPos[1]));
        move = vec2.fromValues(getNext()[0], getNext()[1]);
    }

    let distance = vec2.distance(santaGrid, playerPosition);// get the distance between player and the santa
    if ((Math.abs(santaPos[0] - move[0]) >= step && Math.abs(santaPos[0] - move[0]) <= (step * 5)) &&
        (Math.abs(santaPos[1] - move[1]) >= step && Math.abs(santaPos[1] - move[1]) <= (step * 5))) {
        //console.log("santa is at " + santaPos);
    }

    if (Math.abs(santaPos[0] - move[0]) <= step && Math.abs(santaPos[1] - move[1]) <= step) {//if it is on the grid
        //round up the santa's position so that there won't be any epsilon for the next grid
        //vec3.round(santaObject.model.position, santaObject.model.position);

        if (map[santaGrid[0] + 1][santaGrid[1]] === "." &&
            vec2.distance(vec2.fromValues(santaGrid[0] + 1, santaGrid[1]), playerPosition) <= distance) {// this is the front
            setNext(santaGrid[0] + 1, santaGrid[1]);//set next move to this grid
            direction = 0;//set the moving direction to 0
            distance = vec2.distance(vec2.fromValues(santaGrid[0] + 1, santaGrid[1]), playerPosition);//refresh the distance
        } else if (map[santaGrid[0]][santaGrid[1] + 1] === "." &&
            vec2.distance(vec2.fromValues(santaGrid[0], santaGrid[1] + 1), playerPosition) <= distance) {// this is the right

            setNext(santaGrid[0], santaGrid[1] + 1);//set next move to this grid
            direction = 1;//set the moving direction to 1
            distance = vec2.distance(vec2.fromValues(santaGrid[0], santaGrid[1] + 1), playerPosition);//refresh the distance
        } else if (map[santaGrid[0] - 1][santaGrid[1]] === "." &&
            vec2.distance(vec2.fromValues(santaGrid[0] - 1, santaGrid[1]), playerPosition) <= distance) {// this is the back

            setNext(santaGrid[0] - 1, santaGrid[1]);//set next move to this grid
            direction = 2;//set the moving direction to 0
            distance = vec2.distance(vec2.fromValues(santaGrid[0] - 1, santaGrid[1]), playerPosition);//refresh the distance
        } else if (map[santaGrid[0]][santaGrid[1] - 1] === "." &&
            vec2.distance(vec2.fromValues(santaGrid[0], santaGrid[1] - 1), playerPosition) <= distance) {// this is the left

            setNext(santaGrid[0], santaGrid[1] - 1);//set next move to this grid
            direction = 3;//set the moving direction to 0
            distance = vec2.distance(vec2.fromValues(santaGrid[0], santaGrid[1] - 1), playerPosition);//refresh the distance
        } else {
            direction = -1;
        }
    }
    //update santaFace
    
    //check if the direction has nothing
    //check if the direction is closer to the player
    //var santaObject = state.objects[findObjectByName(state, "santa")];

    //rotate santa
    return direction;
}


/*all helper functions go here*
 * /
 * @param {any} state
 * @param {string of the target object} name
 */
function findObjectByName(state, name) {
    var index = 0;
    for (var i = 0; i < state.objects.length; i++) {
        if (state.objects.name === name) {
            index = i;
            break;
        }
    }
    return index;
}

//check if santa reaches the player
function checkPlayer(playerGrid, santaPos) {
    var santaGrid = vec2.fromValues(0.0, 0.0, 0.0);
    vec2.round(santaGrid, santaPos);

    return vec2.distance(playerGrid, santaGrid) < 1;
}

function roundTo(num, roundto) {
    return Math.round(num * Math.pow(10, roundto)) / Math.pow(10, roundto);
}
