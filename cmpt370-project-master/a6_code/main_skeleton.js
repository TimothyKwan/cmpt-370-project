/* HOW TO RUN THE GAME - The game must be run using a local webserver to load assets.
   Python must be installed before running.
   "run python -m http.server" in command console while in game directory
   Open a browser to http://localhost:8000/html/
*/

main();

/************************************
 * MAIN
 ************************************/

function main() {

    console.log("Setting up the canvas");

    // Find the canavas tag in the HTML document
    const canvas = document.querySelector("#assignmentCanvas");

    // Initialize the WebGL2 context
    var gl = canvas.getContext("webgl2");

    // Only continue if WebGL2 is available and working
    if (gl === null) {
        printError('WebGL 2 not supported by your browser',
            'Check to see you are using a <a href="https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API#WebGL_2_2" class="alert-link">modern browser</a>.');
        return;
    }

    var state = null;

    fetch('/statefiles/objects.json') //get the JSON file with our objects
        .then((data) => {
            return data.json()
        })
        .then((jData) => {
            var inputTriangles = jData;
            state = setupDrawing(gl, canvas, inputTriangles);           
			
			setupKeypresses(state); // Initialize keyboard input.
			
            console.log("Starting rendering loop");
            startRendering(gl, state);
            playBGM();
            setIsPlaying(1);
        })
        .catch((err) => {
            console.error(err);
        })
}

function getTextures(gl, object) {
    if (object.material.textureID) {
        var texture = gl.createTexture();

        const image = new Image();

        image.onload = function () {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
            gl.texImage2D(
                gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
                gl.UNSIGNED_BYTE,
                image
            );
            gl.bindTexture(gl.TEXTURE_2D, null);
            gl.activeTexture(gl.TEXTURE0);
        }

        image.src = '/statefiles/' + object.material.textureID;
        return texture;
    }
}

function getTextures2(gl, object) {
    // textures that want repeat (Floor)
    if (object.material.textureID) {
        var texture = gl.createTexture();

        const image = new Image();

        image.onload = function () {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
            gl.texImage2D(
                gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
                gl.UNSIGNED_BYTE,
                image
            );
            gl.bindTexture(gl.TEXTURE_2D, null);
            gl.activeTexture(gl.TEXTURE0);
        }

        image.src = '/statefiles/' + object.material.textureID;
        return texture;
    }
}

function setupDrawing(gl, canvas, inputTriangles) {
    // Create a state for our scene
    var state = {
        camera: {
            position: vec3.fromValues(2.5, 0.5, 4.5),
            center: vec3.fromValues(2.5, 0.5, 5.5),
            up: vec3.fromValues(0.0, 1.0, 0.0),
        },
        camera2:{
            position: vec3.fromValues(1.5, 10.0, 1.5), 
            center: vec3.fromValues(1.5, 0.0, 1.5), // lookat
            up: vec3.fromValues(0.0, 0.0, 1.0),
        },
        lights: [
            {
                position: vec3.fromValues(1.5, 2.5, 1.5),
                colour: vec3.fromValues(1.0, 1.0, 1.0),
                strength: 1.5,
            }
        ],
        objects: [],
        canvas: canvas,
        selectedIndex: 0,
        cameraBehaviour: {
            // These dictate how the camera rotates about the origin
            radius: 3.0,
            theta: Math.PI / 2.0,
        },
        // TODO link this to a uniform and update at rendering 
        // you can then use in the shader to test if the curent object has texture or not
        samplerExists: false,
        direction: 0,
		santaFace: 0,
        cameraSwap: false,
        inventory: [], // CHANGED
        map: inputTriangles[0].levelData, // CHANGED test
        newLoc: vec3.fromValues(0,0,0),
        gameOver: false,
        winner: false,
    };
	state.newLoc = vec3.fromValues(state.camera.position[0], state.camera.position[1], state.camera.position[2]);
	
	var data = inputTriangles[0];
	var oCount = 0;
	
	// TODO: Add character here.
	
	console.log(data.levelData);
	console.log("Map Size:" + data.levelData.length + "x" + data.levelData[0].length);	
	
	for (var i = 0; i < data.levelData.length; i++) 
	{
		for (var j = 0; j < data.levelData[i].length; j++) 
		{
			var tri = inputTriangles[getObjectData(data.levelData[i][j])];
            if (tri != null)
			{
				state.objects.push
				(
					{
						model: 
						{
							position: vec3.fromValues(i, 0.0, j),
							rotation: mat4.create(), // Identity matrix
                            scale: vec3.fromValues(1.0, 1.0, 1.0),
						},
						programInfo: textureShader(gl),
						buffers: null,
						materialList: tri.material,
						modelMatrix: mat4.create(),
						name: tri.name,
						centroid: calculateCentroid(tri.vertices),
						// parent: tri.parent,					
                        // TODO: Add reference to texture and initialize, use getTextures(gl, tri)
                        texture: getTextures(gl, tri), // get texture of object
					}
                );
            // passed textures to buffer
			initBuffers(gl, state.objects[oCount], tri.vertices.flat(), tri.normals.flat(),  tri.triangles.flat(), tri.uvs.flat());
			oCount++;
            }
		}
    }
    // create floor
    var tri = inputTriangles[8];
    state.objects.push
	    (
			{
				model: 
				{
                    position: vec3.fromValues(0.0, -0.1, 0.0), // counter Z-Fighting
					rotation: mat4.create(), // Identity matrix
                    scale: vec3.fromValues(1.0, 1.0, 1.0),
				},
				programInfo: textureShader(gl),
				buffers: null,
				materialList: tri.material,
				modelMatrix: mat4.create(),
				name: tri.name,
				centroid: calculateCentroid(tri.vertices),
				// parent: tri.parent,					
                texture: getTextures2(gl, tri), // get texture of object
			}
         );
         
	initBuffers(gl, state.objects[oCount], tri.vertices.flat(), tri.normals.flat(),  tri.triangles.flat(), tri.uvs.flat());
    oCount++;

    // create player
    var tri = inputTriangles[15];
    state.objects.push
	    (
			{
				model: 
				{
                    position: vec3.fromValues(2.0, 1.5, 4.0),
					rotation: mat4.create(), // Identity matrix
                    scale: vec3.fromValues(1.0, 1.0, 1.0),
				},
				programInfo: textureShader(gl),
				buffers: null,
				materialList: tri.material,
				modelMatrix: mat4.create(),
				name: tri.name,
				centroid: calculateCentroid(tri.vertices),
				// parent: tri.parent,					
                texture: getTextures2(gl, tri), // get texture of object
			}
         );
         
	initBuffers(gl, state.objects[oCount], tri.vertices.flat(), tri.normals.flat(),  tri.triangles.flat(), tri.uvs.flat());
	oCount++;

/*
    for (var i = 0; i < inputTriangles.length; i++) 
	{
		
        var tri = inputTriangles[i]; //location of object data
        state.objects.push(
            {
                model: {
                    position: vec3.fromValues(0.0, 0.0, 0.0),
                    rotation: mat4.create(), // Identity matrix
                    scale: vec3.fromValues(1.0, 1.0, 1.0),
                },
                programInfo: textureShader(gl),
                buffers: null,
                materialList: tri.material,
                modelMatrix: mat4.create(),
                name: tri.name,
                centroid: calculateCentroid(tri.vertices),
                //parent: tri.parent,
				level: tri.levelData,
                // TODO: Add reference to texture and initialize
                // use getTextures(gl, tri)
            }
        );
        // TODO include texture coords in the call 
        initBuffers(gl, state.objects[i], tri.vertices.flat(), tri.normals.flat(),  tri.triangles.flat());
    }
*/
    return state;
}

function getObjectData(id)
{
	switch(id)
	{
        case "W": return 1;		
        case "I0": return 2; 
        case "I1": return 3;
        case "I2": return 4;
		case "S": return 5;
        case "D0": return 6;
        case "G": return 7; // 8 is floor
        case "W2": return 9;
        case "D1": return 10;
        case "D2": return 11;
        case "D*": return 12;
        case "B": return 13;
        case "E": return 14; // 15 is player
		default: return null;
	}
}

/************************************
 * RENDERING CALLS
 ************************************/

function startRendering(gl, state) {
    // A variable for keeping track of time between frames
    var then = 0.0;

    // This function is called when we want to render a frame to the canvas
    function render(now) {
        now *= 0.001; // convert to seconds
        const deltaTime = now - then;
        then = now;
		
		// <!> Step event - Things to do every frame.
        //let arm = getObjectByName(state, "wall");
        //mat4.rotateX(arm.model.rotation, arm.model.rotation, 0.8 * deltaTime);
        //add movement for santa
        //patrol(state.map, state.objects[getObjectByName(state, "santa")].model, 0.4 * deltaTime);
		patrol(state, getObjectByName(state, "santa"), 0.4 * deltaTime); // move Santa
		
		var distToPlayerPoint = vec3.distance(state.camera.position, state.newLoc);
		
		//if((state.camera.position[0] != state.newLoc[0]) || (state.camera.position[2] != state.newLoc[2]))
		
		
		if(distToPlayerPoint < 0.017)
		{
			state.camera.position[0] = Math.floor(state.camera.position[0]) + 0.5;
			state.camera.position[2] = Math.floor(state.camera.position[2]) + 0.5;
			state.camera.center[0] = Math.floor(state.camera.center[0]) + 0.5;
			state.camera.center[2] = Math.floor(state.camera.center[2]) + 0.5;
		}
		else
		{
			var moveX = state.newLoc[0] - state.camera.position[0];
			var moveY = state.newLoc[2] - state.camera.position[2];
			state.camera.position[0] += moveX/5;
			state.camera.center[0] += moveX/5;
			state.camera.position[2] += moveY/5;
			state.camera.center[2] += moveY/5;
        }		
		
        if((state.camera2.position[0] != state.newLoc[0]) || (state.camera2.position[2] != state.newLoc[2]))
		{
			var moveX = state.newLoc[0] - state.camera2.position[0];
			var moveY = state.newLoc[2] - state.camera2.position[2];
			state.camera2.position[0] += moveX/5;
			state.camera2.center[0] += moveX/5;
			state.camera2.position[2] += moveY/5;
            state.camera2.center[2] += moveY/5;
            state.lights[0].position[0] = state.camera2.position[0];
            state.lights[0].position[2] = state.camera2.position[2];

        }
        

		
	    
		// Draw our scene
        drawScene(gl, deltaTime, state);

        // Request another frame when this one is done
        if (!state.gameOver && !state.winner) {
            requestAnimationFrame(render);
        } else if (state.gameOver){
            stopBGM();
            if (getPlayed() === 0) {
                playHOHOHO();
                setPlayed(1);
                alert("You get caught by the Santa!!");
                location.reload();
            }
        }
        else if(state.winner){
            stopBGM();
            if (getPlayed() === 0){
                playMERRYCHRISTMAS();
                setPlayed(1);
                alert("You won the game!! Merry Christmas");
                location.reload();
            }
        }
    }

    // Draw the scene
    requestAnimationFrame(render);
}

/**
 * Draws the scene. Should be called every frame
 * 
 * @param  {} gl WebGL2 context
 * @param {number} deltaTime Time between each rendering call
 */
function drawScene(gl, deltaTime, state) {
    // Set clear colour
    // This is a Red-Green-Blue-Alpha colour
    // See https://en.wikipedia.org/wiki/RGB_color_model
    // Here we use floating point values. In other places you may see byte representation (0-255).
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // Depth testing allows WebGL to figure out what order to draw our objects such that the look natural.
    // We want to draw far objects first, and then draw nearer objects on top of those to obscure them.
    // To determine the order to draw, WebGL can test the Z value of the objects.
    // The z-axis goes out of the screen

    // gl.depthMask(false);
    // gl.enable(gl.BLEND);
    // gl.blendFunc(gl.ONE_MINUS_CONSTANT_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    // Clear the color and depth buffer with specified clear colour.
    // This will replace everything that was in the previous frame with the clear colour.
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // TODO sort objects 
    // see lab9 for an example
    var sortedObjects = state.objects.sort((a,b) => {
        var distA = vec3.fromValues(0.0, 0.0, 0.0);
        var distB = vec3.fromValues(0.0, 0.0, 0.0); 
        distA = vec3.distance(state.camera.position, a.model.position);
        distB = vec3.distance(state.camera.position, b.model.position);
        return distB - distA;
    })

    sortedObjects.forEach((object) => {
        // Choose to use our shader
        gl.useProgram(object.programInfo.program);

        // Update uniforms
        {
           
            if (object.materialList.alpha < 1.0) {
                // enable blending and specify blending function 
                // clear depth for correct transparency rendering 
                gl.depthMask(false);
                gl.enable(gl.BLEND);
                gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
            }
            else {
                // enable depth masking and z-buffering
                // specify depth function
                // clear depth with 1.0
                gl.depthMask(true);
                gl.enable(gl.DEPTH_TEST); // Enable depth testing
                gl.depthFunc(gl.LEQUAL); // Near things obscure far things
            }
            
			// Projection Matrix
            var projectionMatrix = mat4.create();
            var fovy = 60.0 * Math.PI / 180.0; // Vertical FOV (in radians).
            var aspect = state.canvas.clientWidth / state.canvas.clientHeight; // Aspect ratio
            var near = 0.1;   // Nearest clipping plane
            var far  = 100.0; // Farthest clipping plane
            			
            mat4.perspective(projectionMatrix, fovy, aspect, near, far); // Create matrix
            gl.uniformMatrix4fv(object.programInfo.uniformLocations.projection, false, projectionMatrix); // Link to uniform

			// View Matrix
            var viewMatrix = mat4.create();
            if(state.cameraSwap){ // CHANGED
                mat4.lookAt(
                    viewMatrix,
                    state.camera2.position,
                    state.camera2.center,
                    state.camera2.up,
                );
            }
            else{
                mat4.lookAt(
                    viewMatrix,
                    state.camera.position,
                    state.camera.center,
                    state.camera.up,
                );
            }

            gl.uniformMatrix4fv(object.programInfo.uniformLocations.view, false, viewMatrix); //Link to uniform

			// Model Matrix
            var modelMatrix = mat4.create();
            var negCentroid = vec3.fromValues(0.0, 0.0, 0.0);
            vec3.negate(negCentroid, object.centroid);

            mat4.translate(modelMatrix, modelMatrix, object.model.position);
            mat4.translate(modelMatrix, modelMatrix, object.centroid);
            mat4.mul(modelMatrix, modelMatrix, object.model.rotation);
            mat4.translate(modelMatrix, modelMatrix, negCentroid);

            //update modelview with parent model view
            if (object.parent) {
                let parentObject = getObjectByName(state, object.parent);
                mat4.mul(modelMatrix, parentObject.modelMatrix, modelMatrix);
            }
            object.modelMatrix = modelMatrix;

            var normalMatrix = mat4.create();
            mat4.invert(normalMatrix, modelMatrix);
            mat4.transpose(normalMatrix, normalMatrix);

            gl.uniformMatrix4fv(object.programInfo.uniformLocations.model, false, modelMatrix);
            gl.uniformMatrix4fv(object.programInfo.uniformLocations.normal, false, normalMatrix);

            // Update camera position
            gl.uniform3fv(object.programInfo.uniformLocations.cameraPosition, state.camera.position);

            //Update lights
            gl.uniform3fv(object.programInfo.uniformLocations.light0Position, state.lights[0].position);
            gl.uniform3fv(object.programInfo.uniformLocations.light0Colour, state.lights[0].colour);
            gl.uniform1f(object.programInfo.uniformLocations.light0Strength, state.lights[0].strength);

            // TODO: Add uniform updates here
            gl.uniform3fv(object.programInfo.uniformLocations.ambientValue, object.materialList.ambient);
            gl.uniform3fv(object.programInfo.uniformLocations.diffuseValue, object.materialList.diffuse);
            gl.uniform3fv(object.programInfo.uniformLocations.specularValue, object.materialList.specular);
            gl.uniform1f(object.programInfo.uniformLocations.nValue, object.materialList.n);
            gl.uniform1f(object.programInfo.uniformLocations.alphaValue, object.materialList.alpha);
        }

        // Draw 
        {
            // Bind the buffer we want to draw
            gl.bindVertexArray(object.buffers.vao);

            
            if (object.texture != null) {
                state.samplerExists = 0;
                // link this variable to uniform 
                gl.uniform1i(object.programInfo.uniformLocations.samplerExists, state.samplerExists);
                // update fragment shader uniform for texture sampler & bind
                // and activate texture
                gl.uniform1i(object.programInfo.uniformLocations.sampler, object.texture);
                gl.bindTexture(gl.TEXTURE_2D, object.texture);
                gl.activeTexture(gl.TEXTURE0);
                
            } else {
                state.samplerExists = 1;
                // link this variable to uniform 
                gl.uniform1i(object.programInfo.uniformLocations.samplerExists, state.samplerExists); 
            }

            // Draw the object
            const offset = 0; // Number of elements to skip before starting
            gl.drawElements(gl.TRIANGLES, object.buffers.numVertices, gl.UNSIGNED_SHORT, offset);
        }

    });
}

/************************************
 * UI EVENTS
 ************************************/

function setupKeypresses(state) 
{		
    document.addEventListener("keyup", (event) => {
        //console.log(event.code); // Shows what key is being pressed.        
        let player = getObjectByName(state, "player");
        switch (event.code) 
        {
            case "KeyA":
                // Move camera to the left.
                if(checkFront(state.camera.position, state.direction + 3, state.map) === ".")
                {
                    if(state.direction == 0)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(1.0, 0.0, 0.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(1.0, 0.0, 0.0));
                    }
                    else if(state.direction == 2)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(-1.0, 0.0, 0.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(-1.0, 0.0, 0.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(-1.0, 0.0, 0.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(-1.0, 0.0, 0.0));
                    }
                    else if(state.direction == 1)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, 1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, 1.0));
                    }
                    else if(state.direction == 3)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, -1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, -1.0));
                    }
                }
                else
                {
                    //console.log("Oof! You hit a wall!");
                }
                break;
            case "KeyD":
                // Move camera to the right.
                if(checkFront(state.camera.position, state.direction + 1, state.map) === ".")
                {
                    if(state.direction == 0)
                        {
                            vec3.add(state.newLoc, state.newLoc, vec3.fromValues(-1.0, 0.0, 0.0));
                            //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(-1.0, 0.0, 0.0));
                            //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(-1.0, 0.0, 0.0));
                            vec3.add(player.model.position,player.model.position, vec3.fromValues(-1.0, 0.0, 0.0));
                        }
                    else if(state.direction == 2)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(1.0, 0.0, 0.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(1.0, 0.0, 0.0));
                    }
                    else if(state.direction == 1)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, -1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, -1.0));
                    }
                    else if(state.direction == 3)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, 1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, 1.0));
                    }
                }
                else
                {
                    //console.log("Oof! You hit a wall!");
                }
                    break;
            case "KeyW":
                // Move camera forward.  								
                if(checkFront(state.camera.position, state.direction, state.map) === ".")
                {
                    if(state.direction == 3)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(1.0, 0.0, 0.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(1.0, 0.0, 0.0));
                    }
                    else if(state.direction == 1)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(-1.0, 0.0, 0.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(-1.0, 0.0, 0.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(-1.0, 0.0, 0.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(-1.0, 0.0, 0.0));
                    }
                    else if(state.direction == 0)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, 1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, 1.0));
                    }
                    else if(state.direction == 2)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, -1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, -1.0));
                    }
                }
                else
                {
                    //console.log("Oof! You hit a wall!");
                }
                break;
            case "KeyS":
                // Move camera backward.
                if(checkFront(state.camera.position, state.direction + 2, state.map) === ".")	
                {
                    if(state.direction == 3)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(-1.0, 0.0, 0.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(-1.0, 0.0, 0.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(-1.0, 0.0, 0.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(-1.0, 0.0, 0.0));
                    }
                    else if(state.direction == 1)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(1.0, 0.0, 0.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(1.0, 0.0, 0.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(1.0, 0.0, 0.0));
                    }
                    else if(state.direction == 0)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, -1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, -1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, -1.0));
                    }
                    else if(state.direction == 2)
                    {
                        vec3.add(state.newLoc, state.newLoc, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.position, state.camera.position, vec3.fromValues(0.0, 0.0, 1.0));
                        //vec3.add(state.camera.center, state.camera.center, vec3.fromValues(0.0, 0.0, 1.0));
                        vec3.add(player.model.position,player.model.position, vec3.fromValues(0.0, 0.0, 1.0));
                    }
                }
                else
                {
                    //console.log("Oof! You hit a wall!");
                }
                break;				
            case "KeyQ":
                //vec3.rotateY(state.camera.center, state.camera.center, state.camera.position, 1.571);
                // Rotate camera to the left.
                if (state.direction > 0) 
                {					
                    vec3.rotateY(state.camera.center, state.camera.center, state.camera.position, Math.PI/2); // CHANGED 1.571 for Math.Pi/2
                    mat4.rotateY(player.model.rotation, player.model.rotation, Math.PI/2);
                    state.direction--;// Increment direction value.
                }
                else 
                {
                    vec3.rotateY(state.camera.center, state.camera.center, state.camera.position, Math.PI/2); // CHANGED
                    mat4.rotateY(player.model.rotation, player.model.rotation, Math.PI/2);
                    state.direction = 3;// Set direction value to 0.
                }
                console.log(state.direction);
                break;
            case "KeyE":
                //vec3.rotateY(state.camera.center, state.camera.center, state.camera.position, -1.571);
                // Rotate camera to the right.
                if (state.direction < 3) 
                {					
                    vec3.rotateY(state.camera.center, state.camera.center, state.camera.position, -Math.PI/2); // CHANGED
                    mat4.rotateY(player.model.rotation, player.model.rotation, -Math.PI/2);
                    state.direction++;// Increment direction value.
                }
                else 
                {
                    vec3.rotateY(state.camera.center, state.camera.center, state.camera.position, -Math.PI/2); // CHANGED
                    mat4.rotateY(player.model.rotation, player.model.rotation, -Math.PI/2);
                    state.direction = 0;// Set direction value to 0.
                }
                console.log(state.direction);
                break;
            case "KeyP":
                console.log("Position: " + state.camera.position);				
                break;
            case "KeyL":
                // swapping cameras
                if (state.cameraSwap){ // return to Player POV (First Person)
                    state.cameraSwap = false;
                }
                else{ // swap to Top Down view from player position, movement will not change camera2 position. Must swap twice to update view
                    state.cameraSwap = true;
                    vec3.set(state.camera2.position, state.camera.position[0], 10.0, state.camera.position[2]); // position
                    vec3.set(state.camera2.center, state.camera.position[0], 0.0, state.camera.position[2]); // lookat
                }
                break;
            case "Space":
                var front = checkFront(state.camera.position, state.direction, state.map) // grab value in map
                var indices = indexInFront(state.camera.position, state.direction); // grab index in front of player
                console.log("Front: " + front);
                
                if (front === "I0"){
                    // update inventory, map, object
                    let itemIndex = state.inventory.length;
                    state.inventory.push("item0");
                    state.map[indices[0]][indices[1]] = ".";
                    for (var i = 0; i < state.objects.length; i++){
                        if (state.objects[i].name === "item0"){
                            state.objects.splice(i, 1); // removes object from list
                            document.getElementById("image" + itemIndex).src = itemToPath(state.inventory[itemIndex]) // add to inventory UI
                        }
                    }
                }
                else if (front === "I1"){
                    // update inventory, map, object
                    let itemIndex = state.inventory.length;
                    state.inventory.push("item1");
                    state.map[indices[0]][indices[1]] = ".";
                    for (var i = 0; i < state.objects.length; i++){
                        if (state.objects[i].name === "item1"){
                            state.objects.splice(i, 1); // removes object from list
                            document.getElementById("image" + itemIndex).src = itemToPath(state.inventory[itemIndex]) // add to inventory UI
                        }   
                    }
                }
                else if (front === "I2"){
                    // update inventory, map, object
                    let itemIndex = state.inventory.length;
                    state.inventory.push("item2");
                    state.map[indices[0]][indices[1]] = ".";
                    for (var i = 0; i < state.objects.length; i++){
                        if (state.objects[i].name === "item2"){
                            state.objects.splice(i, 1); // removes object from list
                            document.getElementById("image" + itemIndex).src = itemToPath(state.inventory[itemIndex]) // add to inventory UI
                        }
                    }
                }
                else if(front === "D0")
                {
                    for(var i = 0; i < state.inventory.length; i++)
                    {
                        if(state.inventory[i] === "item0")
                        {
                            console.log("Door unlocked!");
                            removeTile(state, "D0");
                            state.inventory.splice(i, 1); // remove from array
                            document.getElementById("image" + i).src = "" // remove from inventory UI
                        }
                    }
                }
                else if(front === "D1")
                {
                    for(var i = 0; i < state.inventory.length; i++)
                    {
                        if(state.inventory[i] === "item1")
                        {
                            console.log("Door unlocked!");
                            removeTile(state, "D1");
                            for (var j = 0; j < state.objects.length; j++){
                                if (state.objects[j].name === "door*"){
                                    let tempIndices = [state.objects[j].model.position[0], state.objects[j].model.position[2]];
                                    state.map[tempIndices[0]][tempIndices[1]] = "."; // update map
                                    state.objects.splice(j, 1) // removes object from list
                                }
                            }
                            state.inventory.splice(i, 1); // remove from array
                            document.getElementById("image" + i).src = "" // remove from inventory UI
                        }
                    }
                }
                else if(front === "D2")
                {
                    for(var i = 0; i < state.inventory.length; i++)
                    {
                        if(state.inventory[i] === "item2")
                        {
                            console.log("Door unlocked!");
                            removeTile(state, "D2");
                            state.inventory.splice(i, 1); // remove from array
                            document.getElementById("image" + i).src = "" // remove from inventory UI
                        }
                    }
                }
                else if(front === "B")
                {
                    removeTile(state, "B");
                }
                else if(front === "E")
                {
                    state.winner = true;
                }
                break;
            case "KeyF":
                state.lights[0].position = vec3.fromValues(state.camera.position[0], 2.5 ,state.camera.position[2]);
            break;
            case "KeyB":
                removeTile(state, "W");
                break;
            default:
                break;
    }
		
    });


}

function removeTile(state, id)
{
	if(checkFront(state.camera.position, state.direction, state.map) === id)
	{
		var indices = indexInFront(state.camera.position, state.direction); // grab index in front of player
		state.map[indices[0]][indices[1]] = "."; // update map
		
		for (var i = 0; i < state.objects.length; i++){
			if (state.objects[i].model.position[0] === indices[0] && state.objects[i].model.position[2] == indices[1]){				
				state.objects.splice(i, 1) // removes object from list
			}
		}
	}
	else
	{
		console.log("removeTile didn't see the item that should be destroyed");
	}
}

/************************************
 * SHADER SETUP
 ************************************/

function textureShader(gl) {

    // Vertex shader source code
    const vsSource =
        `#version 300 es
    
    // input/output: texture coords
    in vec2 aUV;
    out vec2 oUV;
	
	// input: attributes
	in vec3 aPosition; //vertex position
    in vec3 aNormal;   //vertex normal
    
	// input: uniforms
    uniform mat4 uProjectionMatrix;
    uniform mat4 uViewMatrix;
    uniform mat4 uModelMatrix;
    uniform mat4 uNormalMatrix;
    uniform vec3 uCameraPosition;
    
    // output
    out vec3 oFragPosition;
	out vec3 oNormal;    
    out vec3 oCameraPosition;
	out vec3 normalInterp;  

    void main() {
        // Position needs to be a vec4 with w as 1.0
        gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aPosition, 1.0);
        
        // Postion of the fragment in world space
        oFragPosition = (uModelMatrix * vec4(aPosition, 1.0)).xyz; //update fragment position
        oNormal = (uNormalMatrix * vec4(aNormal, 0.0)).xyz;        //update normal
        oCameraPosition = uCameraPosition;                         //pass camera position to fragment shader
		
        normalInterp = vec3(uNormalMatrix * vec4(aNormal, 0.0));

        oUV = aUV;
    }
    `;

    // Fragment shader source code
    const fsSource =
        `#version 300 es
    precision highp float;

    // input: texture related
    in vec2 oUV;
    uniform sampler2D uTexture;
    uniform int samplerExists;

	// input: from vertex shader
    in vec3 oNormal;
    in vec3 oFragPosition;
    in vec3 oCameraPosition;
    in vec3 normalInterp;   
    
	// input: uniforms
    uniform vec3 uLight0Colour;    // Colour of the light
	uniform vec3 uLight0Position;  // Light's position	
	uniform float uLight0Strength; // Light strength
	uniform float nVal;
	uniform vec3 ambientVal;
	uniform vec3 diffuseVal;
	uniform vec3 specularVal;
    uniform float alphaVal;
    
	// output
	out vec4 fragColor;
	
    void main() {
        //vec3 normal = normalize(oNormal);
        vec3 normal = normalize(normalInterp);
        
        vec3 lightDirection = normalize(uLight0Position - oFragPosition);
        vec3 cameraDirection = normalize(oCameraPosition - oFragPosition);
		
        //Calculate Ambient light: Ka*La*Lstrength
        vec3 str = vec3(uLight0Strength, uLight0Strength, uLight0Strength);
        vec3 ambient = str * ambientVal * uLight0Colour.rgb;

        //Calculate Diffuse light: Kd*Ld*dot(N, L)
        float diff = max(dot(normal, lightDirection), 0.0);
        vec3 diffuse = diff * diffuseVal * uLight0Colour.rgb;
           
        //Calculate Specular light: Ks*Ls*(HdotN)^(n*n_all)
        vec3 halfVector = normalize(cameraDirection + lightDirection); //midpoint between light and view vectors    
        float spec = pow(max(dot(halfVector, normal), 0.0), nVal);
        vec3 specular = spec * specularVal * uLight0Colour.rgb;

        // Calculate the final colour
        if(samplerExists == 0){
            // texture exists
            vec4 textureColor = texture(uTexture, oUV);
            vec3 diffuseMix = mix(diffuseVal, textureColor.rgb, 0.7);
            fragColor = vec4((ambient + diffuseMix * diffuse + specular), alphaVal);
        }
        else{
            // texture does not exist
            fragColor = vec4((ambient + diffuseVal * diffuse + specular), alphaVal);
        }
        
        //fragColor = vec4((ambient + diffuse), 1.0); //new shading method -specular
		//fragColor = vec4(diffuseVal, alphaVal); //old shading method
      
       
    }
    `;


    // Create our shader program with our custom function
    const shaderProgram = initShaderProgram(gl, vsSource, fsSource);

    // Collect all the info needed to use the shader program.
    const programInfo = {
        // The actual shader program
        program: shaderProgram,
        // The attribute locations. WebGL will use there to hook up the buffers to the shader program.
        // NOTE: it may be wise to check if these calls fail by seeing that the returned location is not -1.
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, 'aPosition'),
            vertexNormal: gl.getAttribLocation(shaderProgram, 'aNormal'),
            // attribute for uv coords
            vertexUV: gl.getAttribLocation(shaderProgram, 'aUV'), 
        },
        uniformLocations: {
            projection: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            view: gl.getUniformLocation(shaderProgram, 'uViewMatrix'),
            model: gl.getUniformLocation(shaderProgram, 'uModelMatrix'),
			normal: gl.getUniformLocation(shaderProgram, 'uNormalMatrix'),
            cameraPosition: gl.getUniformLocation(shaderProgram, 'uCameraPosition'),
            // uniforms for light 
			light0Position: gl.getUniformLocation(shaderProgram, 'uLight0Position'),
			light0Colour: gl.getUniformLocation(shaderProgram, 'uLight0Colour'),
			light0Strength:	gl.getUniformLocation(shaderProgram, 'uLight0Strength'),
            // uniforms for material properties 
            ambientValue: gl.getUniformLocation(shaderProgram, "ambientVal"),
			diffuseValue: gl.getUniformLocation(shaderProgram, "diffuseVal"),
			specularValue: gl.getUniformLocation(shaderProgram, "specularVal"),
			alphaValue: gl.getUniformLocation(shaderProgram, "alphaVal"),
			nValue: gl.getUniformLocation(shaderProgram, "nVal"),
			// uniform for texture sampler
			sampler: gl.getUniformLocation(shaderProgram, "uTexture"),
            // TODO uniform for state.samplerExists
			samplerExists: gl.getUniformLocation(shaderProgram, "samplerExists"),          
        },
    };

    // Check to see if we found the locations of our uniforms and attributes
    // Typos are a common source of failure
    // TODO test all additional uniforms and attributes
    if (programInfo.attribLocations.vertexPosition === -1 ||
        programInfo.attribLocations.vertexNormal === -1 ||
        programInfo.attribLocations.vertexUV === -1 ||
        programInfo.uniformLocations.projection === -1 ||
        programInfo.uniformLocations.view === -1 ||
        programInfo.uniformLocations.model === -1 ||
		programInfo.uniformLocations.normal === -1 ||
		programInfo.uniformLocations.cameraPosition === -1 ||
		programInfo.uniformLocations.light0Position === -1 ||
		programInfo.uniformLocations.light0Colour === -1 ||
		programInfo.uniformLocations.light0Strength === -1 ||
		programInfo.uniformLocations.ambientValue === -1 ||
        programInfo.uniformLocations.diffuseValue === -1 ||
		programInfo.uniformLocations.specularValue === -1 ||        
        programInfo.uniformLocations.alphaValue === -1 ||
        programInfo.uniformLocations.nValue === -1 ||
        programInfo.uniformLocations.sampler === -1 ||
        programInfo.uniformLocations.samplerExists === -1) { 
        printError('Shader Location Error', 'One or more of the uniform and attribute variables in the shaders could not be located');
    }

    return programInfo;
}

/************************************
 * BUFFER SETUP
 ************************************/
// TODO add texture coords array as argument 
function initBuffers(gl, object, positionArray, normalArray, indicesArray, textureCoordsArray) {

    // We have 3 vertices with x, y, and z values
    const positions = new Float32Array(positionArray);

    const normals = new Float32Array(normalArray);

    // array for texture coordinates 
    const textureCoords = new Float32Array(textureCoordsArray);
  
    // We are using gl.UNSIGNED_SHORT to enumerate the indices
    const indices = new Uint16Array(indicesArray);

    // Allocate and assign a Vertex Array Object to our handle
    var vertexArrayObject = gl.createVertexArray();

    // Bind our Vertex Array Object as the current used object
    gl.bindVertexArray(vertexArrayObject);

    object.buffers = {
        vao: vertexArrayObject,
        attributes: {
            position: initPositionAttribute(gl, object.programInfo, positions),
            normal: initNormalAttribute(gl, object.programInfo, normals),
            // init uv buffer using initTextureCoords function
            uv: initTextureCoords(gl, object.programInfo, textureCoords),
        },
        indices: initIndexBuffer(gl, indices),
        numVertices: indices.length,
    };
}

function initPositionAttribute(gl, programInfo, positionArray) {

    // Create a buffer for the positions.
    const positionBuffer = gl.createBuffer();

    // Select the buffer as the one to apply buffer
    // operations to from here out.
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    // Now pass the list of positions into WebGL to build the
    // shape. We do this by creating a Float32Array from the
    // JavaScript array, then use it to fill the current buffer.
    gl.bufferData(
        gl.ARRAY_BUFFER, // The kind of buffer this is
        positionArray, // The data in an Array object
        gl.STATIC_DRAW // We are not going to change this data, so it is static
    );

    // Tell WebGL how to pull out the positions from the position
    // buffer into the vertexPosition attribute.
    {
        const numComponents = 3; // pull out 3 values per iteration, ie vec3
        const type = gl.FLOAT; // the data in the buffer is 32bit floats
        const normalize = false; // don't normalize between 0 and 1
        const stride = 0; // how many bytes to get from one set of values to the next
        // Set stride to 0 to use type and numComponents above
        const offset = 0; // how many bytes inside the buffer to start from


        // Set the information WebGL needs to read the buffer properly
        gl.vertexAttribPointer(
            programInfo.attribLocations.vertexPosition,
            numComponents,
            type,
            normalize,
            stride,
            offset
        );
        // Tell WebGL to use this attribute
        gl.enableVertexAttribArray(
            programInfo.attribLocations.vertexPosition);
    }

    return positionBuffer;
}

function initNormalAttribute(gl, programInfo, normalArray) {

    // Create a buffer for the positions.
    const normalBuffer = gl.createBuffer();

    // Select the buffer as the one to apply buffer
    // operations to from here out.
    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);

    // Now pass the list of positions into WebGL to build the
    // shape. We do this by creating a Float32Array from the
    // JavaScript array, then use it to fill the current buffer.
    gl.bufferData(
        gl.ARRAY_BUFFER, // The kind of buffer this is
        normalArray, // The data in an Array object
        gl.STATIC_DRAW // We are not going to change this data, so it is static
    );

    // Tell WebGL how to pull out the positions from the position
    // buffer into the vertexPosition attribute.
    {
        const numComponents = 3; // pull out 4 values per iteration, ie vec3
        const type = gl.FLOAT; // the data in the buffer is 32bit floats
        const normalize = false; // don't normalize between 0 and 1
        const stride = 0; // how many bytes to get from one set of values to the next
        // Set stride to 0 to use type and numComponents above
        const offset = 0; // how many bytes inside the buffer to start from

        // Set the information WebGL needs to read the buffer properly
        gl.vertexAttribPointer(
            programInfo.attribLocations.vertexNormal,
            numComponents,
            type,
            normalize,
            stride,
            offset
        );
        // Tell WebGL to use this attribute
        gl.enableVertexAttribArray(
            programInfo.attribLocations.vertexNormal);
    }

    return normalBuffer;
}


function initTextureCoords(gl, programInfo, textureCoords) {
    if (textureCoords != null && textureCoords.length > 0) {
        // Create a buffer for the positions.
        const textureCoordBuffer = gl.createBuffer();

        // Select the buffer as the one to apply buffer
        // operations to from here out.
        gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);

        // Now pass the list of positions into WebGL to build the
        // shape. We do this by creating a Float32Array from the
        // JavaScript array, then use it to fill the current buffer.
        gl.bufferData(
            gl.ARRAY_BUFFER, // The kind of buffer this is
            textureCoords, // The data in an Array object
            gl.STATIC_DRAW // We are not going to change this data, so it is static
        );

        // Tell WebGL how to pull out the positions from the position
        // buffer into the vertexPosition attribute.
        {
            const numComponents = 2;
            const type = gl.FLOAT; // the data in the buffer is 32bit floats
            const normalize = false; // don't normalize between 0 and 1
            const stride = 0; // how many bytes to get from one set of values to the next
            // Set stride to 0 to use type and numComponents above
            const offset = 0; // how many bytes inside the buffer to start from

            // Set the information WebGL needs to read the buffer properly
            gl.vertexAttribPointer(
                programInfo.attribLocations.vertexUV,
                numComponents,
                type,
                normalize,
                stride,
                offset
            );
            // Tell WebGL to use this attribute
            gl.enableVertexAttribArray(
                programInfo.attribLocations.vertexUV);
        }

        return textureCoordBuffer;
    }
}

function initIndexBuffer(gl, elementArray) {

    // Create a buffer for the positions.
    const indexBuffer = gl.createBuffer();

    // Select the buffer as the one to apply buffer
    // operations to from here out.
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

    // Now pass the list of positions into WebGL to build the
    // shape. We do this by creating a Float32Array from the
    // JavaScript array, then use it to fill the current buffer.
    gl.bufferData(
        gl.ELEMENT_ARRAY_BUFFER, // The kind of buffer this is
        elementArray, // The data in an Array object
        gl.STATIC_DRAW // We are not going to change this data, so it is static
    );

    return indexBuffer;
}

function indexInFront(position, direction){
    var xIndex = Math.floor(position[0]);
    var zIndex = Math.floor(position[2]);
    if (direction === 0){
        return [xIndex, zIndex + 1];
    }
    else if (direction === 1){
        return [xIndex - 1, zIndex];
    }
    else if (direction === 2){
        return [xIndex, zIndex - 1];
    }
    else if (direction === 3){
        return [xIndex + 1, zIndex];
    }
}

function checkFront(position, direction, levelData){
    // checks the front of player for object 
    // returns the value in that position
	
	if(direction > 3) direction -= 4;
	else if(direction < 0) direction += 4;
	console.log(direction);
	
    var indices = indexInFront(position, direction);
    var xIndex = Math.floor(indices[0]);
    var zIndex = Math.floor(indices[1]);
    if (direction === 0){
        return levelData[xIndex][zIndex];
    }
    else if (direction === 1){
        return levelData[xIndex][zIndex];
    }
    else if (direction === 2){
        return levelData[xIndex][zIndex];
    }
    else if (direction === 3){
        return levelData[xIndex][zIndex];
    }



}
function itemToPath(item){
    // convert item string to its path
    var path = "../statefiles/";
    path += item;
    path += ".jpg"; // ../statefiles/item0
    return path;
}
