//check if the game game bgm is playered
var played = 0;
//check if bgm is playering
var isPlaying = 0;
function playBGM() {
    var audio = document.getElementById("BGM");
    if (audio != null) {
        audio.autoplay = true;
        audio.load();
        audio.loop = true;
        //console.log("audio is not null");
    }
}

function stopBGM() {
    var audio = document.getElementById("BGM");
    if (audio != null) {
        audio.autoplay = false;
        audio.load();
        //console.log("audio is not null");
    }
}

function playHOHOHO() {
    var audio = document.getElementById("HOHOHO");
    if (audio != null) {
        audio.autoplay = true;
        audio.load();
        //console.log("audio is not null");
    }
}

function playMERRYCHRISTMAS() {
    var audio = document.getElementById("MERRYCHRISTMAS");
    if (audio != null) {
        audio.autoplay = true;
        audio.load();
        //console.log("audio is not null");
    }
}

function getIsPlaying() {
    return isPlaying;
}

function setIsPlaying(status) {
    isPlaying = status;
}

function getPlayed() {
    return played;
}

function setPlayed(status) {
    played = status;
}

/*function printError(error, explicit) {
    console.log(`[${explicit ? 'EXPLICIT' : 'INEXPLICIT'}] ${error.name}: ${error.message}`);
}*/
